//------------------------------------------------------------------------------
//
// MainWindow.cpp created by Yyhrs 2021/02/27
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QtConcurrent>

#include <core.tpp>
#include <operators.hpp>
#include <SApplication.hpp>

#include "MainWindow.hpp"

MainWindow::MainWindow(QWidget *parent):
	QMainWindow{parent},
	m_watto{":/watto.gif"}
{
	sApp->setTheme(m_settings.value(SApplication::Theme, Theme::c_dark).toString());
	sApp->startSplashScreen(Logos::c_shrademnCompanyRed.arg(Theme::c_light));
	setupUi(this);
	movieLabel->setMovie(&m_watto);
	texturesWeightWidget->addColumns(QList{Model, Raw, Weighted, Textures});
	texturesWeightWidget->hideColumn(LogWidget::Time);
	texturesWeightWidget->hideColumn(LogWidget::Log);
	texturesMissingWidget->addColumns(QList{Model, Texture});
	texturesMissingWidget->hideColumn(LogWidget::Time);
	texturesMissingWidget->hideColumn(LogWidget::Log);
	texturesUnusedWidget->addColumns(QList{Texture});
	texturesUnusedWidget->hideColumn(LogWidget::Time);
	texturesUnusedWidget->hideColumn(LogWidget::Log);
	connectActions();
	connectWidgets();
	connect(&m_scannerWatcher, &QFutureWatcherBase::progressRangeChanged, scannerProgressBar, &ProgressBarButton::setRange);
	connect(&m_scannerWatcher, &QFutureWatcherBase::progressValueChanged, scannerProgressBar, &ProgressBarButton::setValue);
	connect(&m_scannerWatcher, &QFutureWatcherBase::resultReadyAt, this, [this](int resultIndex)
	{
		m_reports << m_scannerWatcher.resultAt(resultIndex);
	});
	connect(&m_scannerWatcher, &QFutureWatcherBase::finished, [this]
	{
		QMap<QString, qsizetype> textureUsages;

		for (auto const &report: qAsConst(m_reports))
			for (auto const &texture: report.textures)
				textureUsages[texture] = textureUsages.value(texture, 0) + 1;
		for (auto const &report: qAsConst(m_reports))
		{
			double raw{0};
			double weighted{0};

			for (auto const &texture: report.textures)
			{
				raw += m_textures[texture];
				weighted += m_textures[texture] / textureUsages[texture];
			}
			if (!report.textures.isEmpty())
				texturesWeightWidget->addLog((report.missingTextures.isEmpty() ? LogWidget::Information : LogWidget::Warning), {}, {report.model, raw / 1000000., weighted / 1000000., report.textures.values().join(';')});
			for (auto const &texture: report.missingTextures)
				texturesMissingWidget->addLog(LogWidget::Warning, {}, {report.model, texture});
		}
		for (auto const &texture: keys(m_textures))
			if (!textureUsages.contains(texture))
				texturesUnusedWidget->addLog(LogWidget::Information, {}, {texture});
		texturesMissingWidget->resizeColumnsToContents();
		stackedWidget->setCurrentIndex(1);
		m_watto.stop();
		scannerProgressBar->resetProgressBar();
	});
	connect(&m_scrapperWatcher, &QFutureWatcherBase::progressRangeChanged, scrapperProgressBar, &ProgressBarButton::setRange);
	connect(&m_scrapperWatcher, &QFutureWatcherBase::progressValueChanged, scrapperProgressBar, &ProgressBarButton::setValue);
	connect(&m_scrapperWatcher, &QFutureWatcherBase::finished, scrapperProgressBar, &ProgressBarButton::resetProgressBar);
	connect(&m_scrapperWatcher, &QFutureWatcherBase::finished, textureListWidget, &PathListWidget::clear);
	m_settings.restoreState(this);
	modelsPathLineEdit->setText(m_settings.value(ModelsPath).toString());
	texturesPathLineEdit->setText(m_settings.value(TexturesPath).toString());
	texconvPathLineEdit->setText(m_settings.value(TexconvPath).toString());
	textureListWidget->setOpenOption(PathListWidget::Files, "*.dds");
	textureListWidget->setBehaviour(PathListWidget::RecursiveFiles);
	for (int value: {2048, 1024, 512, 256, 128, 64})
		sizeComboBox->addItem(QString{"%1x%1"}.arg(value), QStringList{"-w", QString::number(value), "-h", QString::number(value)});
	sApp->stopSplashScreen(this);
}

MainWindow::~MainWindow()
{
	m_settings.saveState(this);
	m_settings.setValue(ModelsPath, modelsPathLineEdit->text());
	m_settings.setValue(TexturesPath, texturesPathLineEdit->text());
	m_settings.setValue(TexconvPath, texconvPathLineEdit->text());
}

void MainWindow::connectActions()
{
	addAction(actionCycleTheme);
	connect(actionCycleTheme, &QAction::triggered, this, &SApplication::cycleTheme);
}

void MainWindow::connectWidgets()
{
	connect(modelsPushButton, &QPushButton::clicked, this, [this]
	{
		modelsPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(modelsPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		scannerProgressBar->setDisabled(text.isEmpty() || texturesPathLineEdit->text().isEmpty());
	});
	connect(texturesPushButton, &QPushButton::clicked, this, [this]
	{
		texturesPathLineEdit->openBrowser(PathLineEdit::Folder);
	});
	connect(texturesPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		scannerProgressBar->setDisabled(text.isEmpty() || modelsPathLineEdit->text().isEmpty());
	});
	connect(scannerProgressBar, &ProgressBarButton::clicked, this, [this]
	{
		QFile vanilla{":/vanilla.txt"};

		vanilla.open(QFile::ReadOnly);
		for (auto const &texture: QString{vanilla.readAll()}.split(QChar::LineFeed))
		{
			qDebug() << texture;
			m_textures[texture] = 0;
		}
		vanilla.close();
		m_watto.start();
		scannerProgressBar->setFormat("%v / %m");
		m_files = File::getFiles(modelsPathLineEdit->text(), {"*.alo"});
		for (auto const &file: QDir{texturesPathLineEdit->text()}.entryInfoList({"*.dds", "*.tga", "*.png", "*.jpg"}))
			m_textures[file.fileName().toLower()] = file.size();
		m_reports.clear();
		stackedWidget->setCurrentIndex(0);
		texturesWeightWidget->clear();
		texturesMissingWidget->clear();
		texturesUnusedWidget->clear();
		m_scannerWatcher.setFuture(QtConcurrent::mapped(m_files, std::function<Report(QFileInfo const &)>{[this](QFileInfo const &file)
			{
				return getReport(file);
			}}));
	});
	connect(texconvPushButton, &QPushButton::clicked, this, [this]
	{
		texconvPathLineEdit->openBrowser(PathLineEdit::File, "texconv.exe");
	});
	connect(texconvPathLineEdit, &PathLineEdit::textChanged, this, [this](QString const &text)
	{
		scrapperProgressBar->setDisabled(text.isEmpty() || textureListWidget->count() == 0);
	});
	connect(addTexturesPushButton, &QPushButton::clicked, this, [this]
	{
		textureListWidget->actions()[PathListWidget::Add]->trigger();
	});
	connect(textureListWidget, &PathListWidget::listModified, this, [this]
	{
		scrapperProgressBar->setDisabled(texconvPathLineEdit->text().isEmpty() || textureListWidget->count() == 0);
	});
	connect(scrapperProgressBar, &ProgressBarButton::clicked, this, [this]
	{
		scrapperProgressBar->setFormat("%v / %m");
		m_files = {};
		for (int row{0}; row < textureListWidget->count(); ++row)
			m_files << textureListWidget->item(row)->text();
		m_scrapperWatcher.setFuture(QtConcurrent::map(m_files, [this](QFileInfo const &file)
		{
			QProcess    process;
			QStringList arguments{"-y", "-sepalpha", "-o", file.absolutePath(), file.absoluteFilePath()};

			arguments << sizeComboBox->currentData().toStringList();
			process.start(texconvPathLineEdit->text(), arguments);
			process.waitForFinished();
		}));
	});
}

MainWindow::Report MainWindow::getReport(QFileInfo const &file) const
{
	Report report;

	report.model = file.completeBaseName();
	try
	{
		Alamo::Model model{file, {}};

		for (quint64 index = 0; index < model.meshCount(); ++index)
			for (auto const &subMesh: model.mesh(index).subMeshes)
				for (auto const &parameter: subMesh.parameters)
				{
					auto const texture{parameter.texture.toLower()};

					qDebug() << __FUNCTION__ << __LINE__ << texture;
					if (!texture.isEmpty() && !report.textures.contains(texture))
					{
						report.textures << texture;
						if (!m_textures.contains(texture))
							report.missingTextures << texture;
					}
				}
	}
	catch (...)
	{
		qWarning() << __FUNCTION__ << __LINE__ << file;
	}
	return report;
}
