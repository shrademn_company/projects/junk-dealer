//------------------------------------------------------------------------------
//
// MainWindow.hpp created by Yyhrs 2021/02/27
//
//------------------------------------------------------------------------------

#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QFutureWatcher>
#include <QMovie>

#include <Animations.h>
#include <Models.h>
#include <Settings.hpp>

#include "ui_MainWindow.h"

class MainWindow: public QMainWindow, private Ui::MainWindow
{
	Q_OBJECT

public:
	enum ReportColumn
	{
		Model,
		Raw,
		Weighted,
		Textures,
		Texture,
		Ticks,
		Proxy,
		Bone,
		Show,
		Hide,
		Total,
		FPS
	};
	Q_ENUM(ReportColumn)
	enum SettingsKey
	{
		ModelsPath,
		TexturesPath,
		TexconvPath
	};
	Q_ENUM(SettingsKey)

	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private:
	struct Report
	{
		QString       model;
		QSet<QString> textures;
		QSet<QString> missingTextures;
	};

	void connectActions();
	void connectWidgets();

	Report getReport(QFileInfo const &file) const;

	Settings                 m_settings;
	QMovie                   m_watto;
	QFutureWatcher<Report>   m_scannerWatcher;
	QFutureWatcher<void>     m_scrapperWatcher;
	QFileInfoList            m_files;
	QMap<QString, qsizetype> m_textures;
	QList<Report>            m_reports;
};

#endif // MAINWINDOW_HPP
